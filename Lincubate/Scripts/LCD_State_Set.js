﻿function LCD_State_Set(wsUrl, s) {
    var soapRequest = '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<soap:Body>' +
                '<LCD_State_Set  xmlns="https://lincubate.org/">' +
                   '<LCDState>' + s + '</LCDState>' +
                '</LCD_State_Set >' +
            '</soap:Body>' +
        '</soap:Envelope>';

    $.ajax({
        type: "POST",
        url: wsUrl,
        contentType: "text/xml",
        dataType: "xml",
        data: soapRequest,
        success: function (data, status) {
            console.log("Update State Status: " + status);
            LCD_State_Get(wsUrl);
        },
        error: function (data, status) {
            console.log("Update State Status: " + status);
        }
    });
};