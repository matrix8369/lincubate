﻿function LCD_Text_Set(wsUrl, t) {
    var soapRequest = '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<soap:Body>' +
                '<LCD_Text_Set  xmlns="https://lincubate.org/">' +
                   '<LCDText>' + t + '</LCDText>' +
                '</LCD_Text_Set >' +
            '</soap:Body>' +
        '</soap:Envelope>';

    $.ajax({
        type: "POST",
        url: wsUrl,
        contentType: "text/xml",
        dataType: "xml",
        data: soapRequest,
        success: function (data, status) {
            console.log("Update Text Status: " + status);
            LCD_Text_Get(wsUrl);
        },
        error: function (data, status) {
            console.log("Update Text Status: " + status);
        }
    });
};