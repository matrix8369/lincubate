﻿function LCD_State_Get(wsUrl) {
    var soapRequest = '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<soap:Body>' +
                '<LCD_State_Get  xmlns="https://lincubate.org/" />' +
            '</soap:Body>' +
        '</soap:Envelope>';

    $.ajax({
        type: "POST",
        url: wsUrl,
        contentType: "text/xml",
        dataType: "xml",
        data: soapRequest,
        success: function (data) {
            $("#LCD_State").val($(data).find("State")[0].innerHTML);
        },
        error: function (data, status) {
            console.log("Status: " + status);
        }
    });
};